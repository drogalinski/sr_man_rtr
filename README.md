# Manipulator RTR

## Opis projektu

Założeniem projektowym jest stworzenie manipulatora RTR. Ruch dwóch pierwszych stopni swobody będzie realizowany za pomocą silników prądu stałego z wbudowanymi enkoderami. Ruch trzeciego stopnia swobody będzie możliwy dzięki zastosowaniu serwomechanizmu. Zamykanie oraz otwieranie chwytaka będzie wykonywane poprzez ruch drugiego serwomechanizmu. Na chwytaku będzie umieszczony czujnik nacisku (tensometr) do wyznaczania siły chwytu. Ruch pierwszego oraz drugiego stopnia swobody będzie ograniczony przez mechaniczne wyłączniki krańcowe. Manipulator ma działać w trybie półautomatycznym. Po podstawieniu elementu ma rozpoznać jego kolor i wrzucić do odpowiedniego pojemnika. Cyfrowe czujniki odległości mają potwierdzać oraz zliczać ile elementów wpadło do danego pojemnika. Kolorowy wyświetlacz ma pokazywać ile elementów jest w danych pojemnikach oraz jakiego koloru jest aktualnie przenoszony element. Mikroprocesor ma za zadanie liczyć kinematykę odwrotną. Sterowanie oraz odbieranie danych będzie realizowane za pośrednictwem interfejsu UART.

## Milestones

- [x] Złożenie części mechanicznej manipulatora, wykonanie połączeń elektrycznych procesora z urządze-
niami peryferyjnymi.
- [x] Zaprogramowanie ruchu ręcznego sterowanego przez interfejs UART. Odczyt danych z następują-
cych czujników:
    - [x] wyłączniki krańcowe,
    - [x] czujniki odległości,
    - [x] czujnik kolorów,
    - [x] czujnik nacisku,
    - [x] enkodery.
- [x] Implementacja algorytmu kinematyki odwrotnej.
- [x] Zaprogramowanie trybu półautomatycznego oraz ~~wyświetlacza~~ trajaktorii ruchu.
