#ifndef INC_PID_H_
#define INC_PID_H_

#include <stdint.h>
#include "stm32l4xx_hal.h"

typedef struct
{
	float proportional;
	float integral;
	float derivative;
	float control_signal;

	float previous_time;
	float previous_error;
	float error_integral;
	float current_time;
	float delta_time;
	float error_value;
	float edot;

	int motor_direction;
	int PWM_value;
	int max_control_signal;
	int min_control_signal;

} pid;

void pid_init(pid *PID, float P, float I, float D, int min_control_signal, int max_control_signal);
void calculate_PID(pid *PID, int16_t current_position, int16_t target_position);

#endif /* INC_PID_H_ */
