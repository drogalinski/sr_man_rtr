#ifndef INC_CONTROL_H_
#define INC_CONTROL_H_

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include "gpio.h"
#include "communication.h"
#include "stm32l4xx_hal.h"
#include "motors.h"
#include "servos.h"
#include "pid.h"
#include "TCS3200.h"

#define ARM_LENGTH 117.5

typedef struct
{
	int q1_target_pos;
	int q2_target_pos;
	int q3_target_pos;

	float recieved_X;
	float recieved_Z;
	int recieved_fi;

} inverse_kinematics;

extern inverse_kinematics Inverse_Kinematics_Data;

void parse_inverse_kinematics_data(uint8_t *buffer_rx, inverse_kinematics *Inverse_Kinematics_Data);
void calculate_inverese_kinematics(inverse_kinematics *Inverse_Kinematics_Data);
void moveQ1(pid *PID);
void moveQ2(pid *PID);
void moveQ3(int target_pos);

void automatic_mode(pid *PID_Q1, pid *PID_Q2, data_to_transmit *Data, inverse_kinematics *Inverse_Kinematics_Data, uint8_t *buffer_tx, uint8_t *tmp_buffer_tx, CRC_HandleTypeDef *hcrc);
void trajectory_mode(pid *PID_Q1, pid *PID_Q2, data_to_transmit *Data, inverse_kinematics *Inverse_Kinematics_Data, uint8_t *buffer_tx, uint8_t *tmp_buffer_tx, CRC_HandleTypeDef *hcrc);

#endif /* INC_CONTROL_H_ */
