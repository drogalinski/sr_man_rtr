/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ADC_FSR_402_Pin GPIO_PIN_0
#define ADC_FSR_402_GPIO_Port GPIOC
#define GRIPPER_SERVO_PWM_Pin GPIO_PIN_0
#define GRIPPER_SERVO_PWM_GPIO_Port GPIOA
#define _3DOF_SERVO_PWM_Pin GPIO_PIN_1
#define _3DOF_SERVO_PWM_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define _1DOF_ENCODER_A_Pin GPIO_PIN_6
#define _1DOF_ENCODER_A_GPIO_Port GPIOA
#define _1DOF_ENCODER_B_Pin GPIO_PIN_7
#define _1DOF_ENCODER_B_GPIO_Port GPIOA
#define IN1_L298N_Pin GPIO_PIN_4
#define IN1_L298N_GPIO_Port GPIOC
#define IN2_L298N_Pin GPIO_PIN_5
#define IN2_L298N_GPIO_Port GPIOC
#define IN3_L298N_Pin GPIO_PIN_0
#define IN3_L298N_GPIO_Port GPIOB
#define IN4_L298N_Pin GPIO_PIN_1
#define IN4_L298N_GPIO_Port GPIOB
#define E18DNK_R_Pin GPIO_PIN_13
#define E18DNK_R_GPIO_Port GPIOB
#define E18DNK_R_EXTI_IRQn EXTI15_10_IRQn
#define E18DNK_G_Pin GPIO_PIN_14
#define E18DNK_G_GPIO_Port GPIOB
#define E18DNK_G_EXTI_IRQn EXTI15_10_IRQn
#define E18DNK_B_Pin GPIO_PIN_15
#define E18DNK_B_GPIO_Port GPIOB
#define E18DNK_B_EXTI_IRQn EXTI15_10_IRQn
#define TCS3200_INPUT_Pin GPIO_PIN_9
#define TCS3200_INPUT_GPIO_Port GPIOC
#define _1DOF_MOTOR_PWM_Pin GPIO_PIN_8
#define _1DOF_MOTOR_PWM_GPIO_Port GPIOA
#define _2DOF_MOTOR_PWM_Pin GPIO_PIN_9
#define _2DOF_MOTOR_PWM_GPIO_Port GPIOA
#define TCS3200_S0_Pin GPIO_PIN_10
#define TCS3200_S0_GPIO_Port GPIOA
#define TCS3200_S1_Pin GPIO_PIN_11
#define TCS3200_S1_GPIO_Port GPIOA
#define TCS3200_S2_Pin GPIO_PIN_12
#define TCS3200_S2_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define TCS3200_S3_Pin GPIO_PIN_15
#define TCS3200_S3_GPIO_Port GPIOA
#define WK625_R_Pin GPIO_PIN_10
#define WK625_R_GPIO_Port GPIOC
#define WK625_L_Pin GPIO_PIN_11
#define WK625_L_GPIO_Port GPIOC
#define WK625_U_Pin GPIO_PIN_12
#define WK625_U_GPIO_Port GPIOC
#define WK625_D_Pin GPIO_PIN_2
#define WK625_D_GPIO_Port GPIOD
#define _2DOF_ENCODER_A_Pin GPIO_PIN_6
#define _2DOF_ENCODER_A_GPIO_Port GPIOB
#define _2DOF_ENCODER_B_Pin GPIO_PIN_7
#define _2DOF_ENCODER_B_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
