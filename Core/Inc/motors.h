#ifndef INC_MOTORS_H_
#define INC_MOTORS_H_

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include "gpio.h"
#include "communication.h"
#include "stm32l4xx_hal.h"

typedef struct
{
	uint8_t speed;
	uint32_t pwm_channel;
	TIM_HandleTypeDef *pwm_htim;
	TIM_HandleTypeDef *encoder_htim;
	volatile int16_t position;
	int16_t start_position;
	int16_t end_position;
	int16_t distance;
	float position_factor;

} motor;

extern motor _1DOF_Motor;
extern motor _2DOF_Motor;

void start_motor_inc(motor *Motor);
void start_motor_dec(motor *Motor);
void set_motor_speed(motor *Motor, uint8_t speed);
void set_motors_speed_rx(uint8_t *buffer_rx);
void stop_motor_soft(motor *Motor);
void stop_motor_hard(motor *Motor);

void update_motor_position(motor *Motor, data_to_transmit *Data);

void manual_motor_control(uint8_t *buffer_rx, data_to_transmit *Data);

void calibrate(data_to_transmit *Data, uint8_t *buffer_tx, uint8_t *tmp_buffer_tx, CRC_HandleTypeDef *hcrc);


#endif /* INC_MOTORS_H_ */
