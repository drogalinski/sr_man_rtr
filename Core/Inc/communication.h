#ifndef INC_COMMUNICATION_H_
#define INC_COMMUNICATION_H_

#define RX_SIZE 64
#define TX_SIZE 64

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include "crc.h"
#include "circ_buff.h"

typedef struct
{
	volatile int16_t q1;
	volatile int16_t q2;
	volatile int8_t q3;
	volatile uint8_t gripper_state;
	volatile uint8_t limit_switch_R;
	volatile uint8_t limit_switch_L;
	volatile uint8_t limit_switch_U;
	volatile uint8_t limit_switch_D;
	volatile uint8_t cnt_r;
	volatile uint8_t cnt_g;
	volatile uint8_t cnt_b;
	volatile uint16_t fsr402_value[2];
	volatile uint8_t detected_color;
	volatile uint16_t crc;

} data_to_transmit;

void update_data_to_transmit(data_to_transmit *Data, uint8_t *buffer_tx, uint8_t *tmp_buffer_tx, CRC_HandleTypeDef *hcrc);


#endif /* INC_COMMUNICATION_H_ */
