#ifndef INC_TCS3200_H_
#define INC_TCS3200_H_

#define TIMCLOCK   80000000
#define PRESCALER  80

#include <stdint.h>
#include <stddef.h>
#include "gpio.h"
#include "communication.h"
#include "stm32l4xx_hal.h"

typedef struct
{

	float frequency;
	uint8_t output_frequency_scale; // 2%, 20%, 100%
	uint8_t active_fotodiode; // 'r', 'g', 'b' or 'c'
	uint32_t input_capture_channel;
	TIM_HandleTypeDef *input_capture_htim;

	volatile uint16_t cnt;

	volatile uint16_t cntR;
	volatile uint16_t cntG;
	volatile uint16_t cntB;

	uint8_t detected_color;

} tcs3200;

extern tcs3200 Color_Sensor;
extern volatile uint32_t IC_Val1;
extern volatile uint32_t IC_Val2;
extern volatile uint32_t Difference;
extern volatile uint8_t Is_First_Captured;
extern volatile uint8_t color_flag;

void set_output_frequency_scale(tcs3200 *TCS3200, uint8_t scale);
void set_filter_diode(tcs3200 *TCS3200, uint8_t diode_type);
void read_frequency(tcs3200 *TCS3200);
void sweep_filters(tcs3200 *TCS3200);
void detect_color(tcs3200 *TCS3200, data_to_transmit *Data);

#endif /* INC_TCS3200_H_ */
