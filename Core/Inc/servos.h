#ifndef INC_SERVOS_H_
#define INC_SERVOS_H_

#define OPEN_GRIPPER_LIMIT 1500
#define CLOSE_GRIPPER_LIMIT 400
#define ANGLE_FACTOR 10.83f

#define _3DOF_START_POS 1560.0f
#define _3DOF_MIN_POS 585.0f
#define _3DOF_MAX_POS 2535.0f

#include <stdint.h>
#include <stddef.h>
#include <math.h>
#include "gpio.h"
#include "communication.h"
#include "stm32l4xx_hal.h"

typedef struct
{
	float raw_angle;
	float pwm_duty;
	uint32_t pwm_channel;
	TIM_HandleTypeDef *pwm_htim;
	uint8_t gripper_flag; // '0' open '1' closed
	int8_t angle;

} servo;

extern servo _GRIPPER_Servo;
extern servo _3DOF_Servo;

void set_3DOF_start_angle(servo *Servo);
void manual_3DOF_control(servo *Servo, uint8_t *buffer_rx);
void update_3DOF_angle(servo *Servo, data_to_transmit *Data);

void open_gripper(servo *Servo, data_to_transmit *Data);
void close_gripper(servo *Servo, data_to_transmit *Data);
void control_close_gripper(servo *Servo, data_to_transmit *Data);
void control_close_gripper_auto(servo *Servo, data_to_transmit *Data);

void manual_gripper_control(uint8_t *buffer_rx, data_to_transmit *Data);

#endif /* INC_SERVOS_H_ */
