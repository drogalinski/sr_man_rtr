#include "pid.h"

void pid_init(pid *PID, float P, float I, float D, int min_control_signal, int max_control_signal)
{
	PID->proportional = P;
	PID->integral = I;
	PID->derivative = D;

	PID->min_control_signal = min_control_signal;
	PID->max_control_signal = max_control_signal;

	PID->control_signal = 0;
	PID->previous_time = 0;
	PID->previous_error = 0;
	PID->error_integral = 0;
	PID->current_time = 0;
	PID->delta_time = 0;
	PID->error_value = 0;
	PID->edot = 0;

	PID->motor_direction = 0;
	PID->PWM_value = 0;

}

void calculate_PID(pid *PID, int16_t current_position, int16_t target_position)
{
	PID->current_time = HAL_GetTick();
	PID->delta_time = (PID->current_time - PID->previous_time) / 1000.0;
	PID->previous_time = PID->current_time;

	PID->error_value = target_position - current_position;

	PID->edot = (PID->error_value - PID->previous_error) / PID->delta_time;

	PID->error_integral = PID->error_integral + (PID->error_value * PID->delta_time);

	PID->control_signal = (PID->proportional * PID->error_value) + (PID->derivative * PID->edot) + (PID->integral * PID->error_integral);

	PID->previous_error = PID->error_value;
}
