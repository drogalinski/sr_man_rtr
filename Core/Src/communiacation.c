#include "communication.h"

void update_data_to_transmit(data_to_transmit *Data, uint8_t *buffer_tx, uint8_t *tmp_buffer_tx, CRC_HandleTypeDef *hcrc)
{
	uint8_t buffer_length = 0;
	memset(tmp_buffer_tx, 0, strlen((char*) tmp_buffer_tx));
	char crc[16] = { 0 };

	sprintf((char*) tmp_buffer_tx, "X %d %d %d %d %d %d %d %d %d %d %d %d %d ", Data->q1, Data->q2, Data->q3, Data->gripper_state, Data->cnt_r, Data->cnt_g, Data->cnt_b, Data->limit_switch_R, Data->limit_switch_L, Data->limit_switch_D, Data->limit_switch_U, Data->fsr402_value[1], Data->detected_color);

	buffer_length = strlen((char*) tmp_buffer_tx);

	Data->crc = HAL_CRC_Calculate(hcrc, (uint32_t*) tmp_buffer_tx, buffer_length);

	sprintf(crc, "%04X\n", Data->crc);

	strcat((char*) tmp_buffer_tx, crc);
	strcpy((char*) buffer_tx, (char*) tmp_buffer_tx);
}
