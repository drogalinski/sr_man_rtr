#include "servos.h"

void set_3DOF_start_angle(servo *Servo)
{
	if (Servo->pwm_channel == TIM_CHANNEL_2)
	{
		__HAL_TIM_SET_COMPARE(Servo->pwm_htim, Servo->pwm_channel, _3DOF_START_POS);
		Servo->angle = 0;
		Servo->pwm_duty = _3DOF_START_POS;
	}
}

void manual_3DOF_control(servo *Servo, uint8_t *buffer_rx)
{
	if (buffer_rx[0] == 'M')
	{

		if (Servo->pwm_channel == TIM_CHANNEL_2)
		{
			switch (buffer_rx[4])
			{
				case '0':
				{
					break;
				}
				case '1':
				{
					if (fabs(Servo->pwm_duty - _3DOF_MIN_POS) > 0.1)
					{
						Servo->pwm_duty = Servo->pwm_duty - 0.05;
						__HAL_TIM_SET_COMPARE(Servo->pwm_htim, Servo->pwm_channel, (uint32_t )Servo->pwm_duty);
					}
					break;
				}
				case '2':
				{
					if (fabs(Servo->pwm_duty - _3DOF_MAX_POS) > 0.1)
					{
						Servo->pwm_duty = Servo->pwm_duty + 0.05;
						__HAL_TIM_SET_COMPARE(Servo->pwm_htim, Servo->pwm_channel, (uint32_t )Servo->pwm_duty);
					}
					break;
				}
				default:
				break;
			}
		}
	}
}

void update_3DOF_angle(servo *Servo, data_to_transmit *Data)
{
	Servo->raw_angle = ((_3DOF_START_POS - Servo->pwm_duty) / ANGLE_FACTOR);
	Servo->angle = (int8_t) Servo->raw_angle;
	Data->q3 = Servo->angle;
}

void open_gripper(servo *Servo, data_to_transmit *Data)
{
	if (Servo->pwm_channel == TIM_CHANNEL_1)
	{
		__HAL_TIM_SET_COMPARE(Servo->pwm_htim, Servo->pwm_channel, OPEN_GRIPPER_LIMIT);
		Servo->pwm_duty = OPEN_GRIPPER_LIMIT;
		Servo->gripper_flag = 0;
		Data->gripper_state = 0;
	}
}

void close_gripper(servo *Servo, data_to_transmit *Data)
{
	if (Servo->pwm_channel == TIM_CHANNEL_1)
	{
		__HAL_TIM_SET_COMPARE(Servo->pwm_htim, Servo->pwm_channel, CLOSE_GRIPPER_LIMIT);
		Servo->pwm_duty = CLOSE_GRIPPER_LIMIT;
	}
}

void control_close_gripper(servo *Servo, data_to_transmit *Data)
{
	if (Servo->pwm_channel == TIM_CHANNEL_1)
	{
		if ((fabs(Servo->pwm_duty - CLOSE_GRIPPER_LIMIT) > 0.1) && (Data->fsr402_value[1] <= 500) && (Servo->gripper_flag == 0))
		{
			Servo->pwm_duty = Servo->pwm_duty - 0.05;
			__HAL_TIM_SET_COMPARE(Servo->pwm_htim, Servo->pwm_channel, (uint32_t )Servo->pwm_duty);
		}
		else
		{
			__HAL_TIM_SET_COMPARE(Servo->pwm_htim, Servo->pwm_channel, (uint32_t )(Servo->pwm_duty+100));
			Servo->gripper_flag = 1;
			Data->gripper_state = 1;
		}
	}
}

void control_close_gripper_auto(servo *Servo, data_to_transmit *Data)
{
	if (Servo->pwm_channel == TIM_CHANNEL_1)
	{
		if ((fabs(Servo->pwm_duty - CLOSE_GRIPPER_LIMIT) > 0.1) && (Data->fsr402_value[1] <= 500) && (Servo->gripper_flag == 0))
		{
			Servo->pwm_duty = Servo->pwm_duty - 0.015;
			__HAL_TIM_SET_COMPARE(Servo->pwm_htim, Servo->pwm_channel, (uint32_t )Servo->pwm_duty);
		}
		else
		{
			__HAL_TIM_SET_COMPARE(Servo->pwm_htim, Servo->pwm_channel, (uint32_t )(Servo->pwm_duty+100));
			Servo->gripper_flag = 1;
			Data->gripper_state = 1;
		}
	}
}

void manual_gripper_control(uint8_t *buffer_rx, data_to_transmit *Data)
{
	if (buffer_rx[0] == 'M')
	{

		switch (buffer_rx[6])
		{
			case '0':
			{
				open_gripper(&_GRIPPER_Servo, Data);

				break;
			}
			case '1':
			{
				//close_gripper(&_GRIPPER_Servo);
				control_close_gripper(&_GRIPPER_Servo, Data);
				break;
			}
			default:
			break;
		}
	}
}
