#include "TCS3200.h"

void set_output_frequency_scale(tcs3200 *TCS3200, uint8_t scale)
{
	switch (scale)
	{
		case 2:
		{
			HAL_GPIO_WritePin(TCS3200_S0_GPIO_Port, TCS3200_S0_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(TCS3200_S1_GPIO_Port, TCS3200_S1_Pin, GPIO_PIN_SET);
			TCS3200->output_frequency_scale = 2;
			break;
		}
		case 20:
		{
			HAL_GPIO_WritePin(TCS3200_S0_GPIO_Port, TCS3200_S0_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(TCS3200_S1_GPIO_Port, TCS3200_S1_Pin, GPIO_PIN_RESET);
			TCS3200->output_frequency_scale = 20;
			break;
		}
		case 100:
		{
			HAL_GPIO_WritePin(TCS3200_S0_GPIO_Port, TCS3200_S0_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(TCS3200_S1_GPIO_Port, TCS3200_S1_Pin, GPIO_PIN_SET);
			TCS3200->output_frequency_scale = 100;
			break;
		}
		default:
			HAL_GPIO_WritePin(TCS3200_S0_GPIO_Port, TCS3200_S0_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(TCS3200_S1_GPIO_Port, TCS3200_S1_Pin, GPIO_PIN_RESET);
			TCS3200->output_frequency_scale = 0;
		break;
	}
}

void set_filter_diode(tcs3200 *TCS3200, uint8_t diode_type)
{
	switch (diode_type)
	{
		case 'r':
		{
			HAL_GPIO_WritePin(TCS3200_S2_GPIO_Port, TCS3200_S2_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(TCS3200_S3_GPIO_Port, TCS3200_S3_Pin, GPIO_PIN_RESET);
			TCS3200->active_fotodiode = 'r';
			break;
		}
		case 'g':
		{
			HAL_GPIO_WritePin(TCS3200_S2_GPIO_Port, TCS3200_S2_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(TCS3200_S3_GPIO_Port, TCS3200_S3_Pin, GPIO_PIN_SET);
			TCS3200->active_fotodiode = 'g';
			break;
		}
		case 'b':
		{
			HAL_GPIO_WritePin(TCS3200_S2_GPIO_Port, TCS3200_S2_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(TCS3200_S3_GPIO_Port, TCS3200_S3_Pin, GPIO_PIN_SET);
			TCS3200->active_fotodiode = 'b';
			break;
		}
		case 'c':
		{
			HAL_GPIO_WritePin(TCS3200_S2_GPIO_Port, TCS3200_S2_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(TCS3200_S3_GPIO_Port, TCS3200_S3_Pin, GPIO_PIN_RESET);
			TCS3200->active_fotodiode = 'c';
			break;
		}
		default:
		break;
	}
}

void read_frequency(tcs3200 *TCS3200)
{
	TCS3200->cnt++;

	if (TCS3200->input_capture_htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4)
	{

		if (Is_First_Captured == 0)
		{
			IC_Val1 = HAL_TIM_ReadCapturedValue(TCS3200->input_capture_htim, TCS3200->input_capture_channel);
			Is_First_Captured = 1;
		}

		else
		{
			IC_Val2 = HAL_TIM_ReadCapturedValue(TCS3200->input_capture_htim, TCS3200->input_capture_channel);

			if (IC_Val2 > IC_Val1)
			{
				Difference = IC_Val2 - IC_Val1;
			}

			else if (IC_Val1 > IC_Val2)
			{
				Difference = (0xffff - IC_Val1) + IC_Val2;
			}

			float refClock = TIMCLOCK / (PRESCALER);

			TCS3200->frequency = refClock / Difference;

			__HAL_TIM_SET_COUNTER(TCS3200->input_capture_htim, TCS3200->input_capture_channel);
			Is_First_Captured = 0;
		}
	}
}

void sweep_filters(tcs3200 *TCS3200)
{
	color_flag++;

	if (color_flag == 1)
	{
		TCS3200->cntR = TCS3200->cnt;
		set_filter_diode(&Color_Sensor, 'g');
	}
	else if (color_flag == 2)
	{
		TCS3200->cntG = TCS3200->cnt;
		set_filter_diode(&Color_Sensor, 'b');
	}
	else if (color_flag == 3)
	{
		TCS3200->cntB = TCS3200->cnt;
		set_filter_diode(&Color_Sensor, 'r');

	}
	else if (color_flag == 4)
	{
		color_flag = 0;
	}

	TCS3200->cnt = 0;
}

void detect_color(tcs3200 *TCS3200, data_to_transmit *Data)
{

	if (TCS3200->cntR > 60 && TCS3200->cntR < 80 && TCS3200->cntG > 17 && TCS3200->cntG < 37 && TCS3200->cntB > 121 && TCS3200->cntB < 150)
	{
		TCS3200->detected_color = 'r';
		Data->detected_color = TCS3200->detected_color;
	}
	else if (TCS3200->cntR > 21 && TCS3200->cntR < 41 && TCS3200->cntG > 39 && TCS3200->cntG < 59 && TCS3200->cntB > 113 && TCS3200->cntB < 149)
	{
		TCS3200->detected_color = 'g';
		Data->detected_color = TCS3200->detected_color;
	}
	else if (TCS3200->cntR > 23 && TCS3200->cntR < 45 && TCS3200->cntG > 50 && TCS3200->cntG < 70 && TCS3200->cntB > 204 && TCS3200->cntB < 240)
	{
		TCS3200->detected_color = 'b';
		Data->detected_color = TCS3200->detected_color;
	}
	else
	{
		TCS3200->detected_color = 'n';
		Data->detected_color = TCS3200->detected_color;
	}
}
