#include "motors.h"

void start_motor_inc(motor *Motor)
{
	if (Motor->pwm_channel == TIM_CHANNEL_1)
	{
		HAL_GPIO_WritePin(IN1_L298N_GPIO_Port, IN1_L298N_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(IN2_L298N_GPIO_Port, IN2_L298N_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(IN3_L298N_GPIO_Port, IN3_L298N_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(IN4_L298N_GPIO_Port, IN4_L298N_Pin, GPIO_PIN_SET);
	}
}

void start_motor_dec(motor *Motor)
{
	if (Motor->pwm_channel == TIM_CHANNEL_1)
	{
		HAL_GPIO_WritePin(IN1_L298N_GPIO_Port, IN1_L298N_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(IN2_L298N_GPIO_Port, IN2_L298N_Pin, GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(IN3_L298N_GPIO_Port, IN3_L298N_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(IN4_L298N_GPIO_Port, IN4_L298N_Pin, GPIO_PIN_RESET);
	}
}

void set_motor_speed(motor *Motor, uint8_t speed)
{
	Motor->speed = speed;

	if (Motor->pwm_channel == TIM_CHANNEL_1)
	{
		__HAL_TIM_SET_COMPARE(Motor->pwm_htim, TIM_CHANNEL_1, speed);
	}
	else
	{
		__HAL_TIM_SET_COMPARE(Motor->pwm_htim, TIM_CHANNEL_2, speed);
	}
}

void set_motors_speed_rx(uint8_t *buffer_rx)
{
	if (buffer_rx[0] == 'S')
	{
		char tmp[16] = { 0 };
		int speed = 0;
		strcpy(tmp, (char*) buffer_rx);
		memmove(tmp, tmp + 2, strlen(tmp));
		sscanf(tmp, "%d", &speed);

		_1DOF_Motor.speed = speed / 2;
		_2DOF_Motor.speed = speed;

		__HAL_TIM_SET_COMPARE(_1DOF_Motor.pwm_htim, TIM_CHANNEL_1, _1DOF_Motor.speed);
		__HAL_TIM_SET_COMPARE(_2DOF_Motor.pwm_htim, TIM_CHANNEL_2, _2DOF_Motor.speed);

	}

}

void stop_motor_soft(motor *Motor)
{
	uint8_t previous_speed = 0;

	if (Motor->pwm_channel == TIM_CHANNEL_1)
	{
		previous_speed = Motor->speed;
		set_motor_speed(&_1DOF_Motor, 0);
		HAL_GPIO_WritePin(IN1_L298N_GPIO_Port, IN1_L298N_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(IN2_L298N_GPIO_Port, IN2_L298N_Pin, GPIO_PIN_SET);
		set_motor_speed(&_1DOF_Motor, previous_speed);
	}
	else
	{
		previous_speed = Motor->speed;
		set_motor_speed(&_2DOF_Motor, 0);
		HAL_GPIO_WritePin(IN3_L298N_GPIO_Port, IN3_L298N_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(IN4_L298N_GPIO_Port, IN4_L298N_Pin, GPIO_PIN_SET);
		set_motor_speed(&_2DOF_Motor, previous_speed);
	}
}

void stop_motor_hard(motor *Motor)
{
	uint8_t previous_speed = 0;

	if (Motor->pwm_channel == TIM_CHANNEL_1)
	{
		previous_speed = Motor->speed;
		set_motor_speed(&_1DOF_Motor, 100);
		HAL_GPIO_WritePin(IN1_L298N_GPIO_Port, IN1_L298N_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(IN2_L298N_GPIO_Port, IN2_L298N_Pin, GPIO_PIN_RESET);
		set_motor_speed(&_1DOF_Motor, previous_speed);
	}
	else
	{
		previous_speed = Motor->speed;
		set_motor_speed(&_2DOF_Motor, 100);
		HAL_GPIO_WritePin(IN3_L298N_GPIO_Port, IN3_L298N_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(IN4_L298N_GPIO_Port, IN4_L298N_Pin, GPIO_PIN_RESET);
		set_motor_speed(&_2DOF_Motor, previous_speed);
	}
}

void update_motor_position(motor *Motor, data_to_transmit *Data)
{
	Motor->position = __HAL_TIM_GET_COUNTER(Motor->encoder_htim);

	if (Motor->pwm_channel == TIM_CHANNEL_1)
	{
		Data->q1 = (Motor->position / Motor->position_factor) - 90;
	}
	else
	{
		Data->q2 = (Motor->position / Motor->position_factor) + 55;
	}

}

void manual_motor_control(uint8_t *buffer_rx, data_to_transmit *Data)
{
	if (buffer_rx[0] == 'M')
	{

		switch (buffer_rx[2])
		{
			case '0':
			{
				stop_motor_hard(&_1DOF_Motor);
				stop_motor_hard(&_2DOF_Motor);
				break;
			}
			case '1':
			{
				if (HAL_GPIO_ReadPin(WK625_R_GPIO_Port, WK625_R_Pin) == GPIO_PIN_SET)
				{
					start_motor_inc(&_1DOF_Motor);
					Data->limit_switch_L = 0;
				}
				else
				{
					stop_motor_hard(&_1DOF_Motor);
					Data->limit_switch_R = 1;
				}
				break;
			}
			case '2':
			{
				if (HAL_GPIO_ReadPin(WK625_L_GPIO_Port, WK625_L_Pin) == GPIO_PIN_SET)
				{
					start_motor_dec(&_1DOF_Motor);
					Data->limit_switch_R = 0;
				}
				else
				{
					stop_motor_hard(&_1DOF_Motor);
					Data->limit_switch_L = 1;
				}
				break;
			}
			case '3':
			{
				if (HAL_GPIO_ReadPin(WK625_U_GPIO_Port, WK625_U_Pin) == GPIO_PIN_SET)
				{
					start_motor_inc(&_2DOF_Motor);
					Data->limit_switch_D = 0;
				}
				else
				{
					stop_motor_hard(&_2DOF_Motor);
					Data->limit_switch_U = 1;
				}
				break;
			}
			case '4':
			{

				if (HAL_GPIO_ReadPin(WK625_D_GPIO_Port, WK625_D_Pin) == GPIO_PIN_SET)
				{
					start_motor_dec(&_2DOF_Motor);
					Data->limit_switch_U = 0;
				}
				else
				{
					stop_motor_hard(&_2DOF_Motor);
					Data->limit_switch_D = 1;
				}
				break;
			}

			default:
			break;
		}
	}
}

void calibrate(data_to_transmit *Data, uint8_t *buffer_tx, uint8_t *tmp_buffer_tx, CRC_HandleTypeDef *hcrc)
{
	// Set motors speed
	set_motor_speed(&_1DOF_Motor, 30);
	set_motor_speed(&_2DOF_Motor, 75);

	// Go to the Q2 minimum position
	start_motor_dec(&_2DOF_Motor);

	while ((HAL_GPIO_ReadPin(WK625_D_GPIO_Port, WK625_D_Pin) == GPIO_PIN_SET))
	{
	}

	// Stop motor
	stop_motor_hard(&_2DOF_Motor);
	Data->limit_switch_D = 1;
	__HAL_TIM_SET_COUNTER(_2DOF_Motor.encoder_htim, 0);
	_2DOF_Motor.start_position = __HAL_TIM_GET_COUNTER(_2DOF_Motor.encoder_htim);
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	HAL_Delay(2000);

	// Go to the Q2 maximum position
	start_motor_inc(&_2DOF_Motor);
	Data->limit_switch_D = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	while ((HAL_GPIO_ReadPin(WK625_U_GPIO_Port, WK625_U_Pin) == GPIO_PIN_SET))
	{
	}

	// Stop motor
	stop_motor_hard(&_2DOF_Motor);
	Data->limit_switch_U = 1;
	HAL_Delay(500);
	_2DOF_Motor.end_position = __HAL_TIM_GET_COUNTER(_2DOF_Motor.encoder_htim);
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	_2DOF_Motor.distance = abs(_2DOF_Motor.start_position) + abs(_2DOF_Motor.end_position);
	_2DOF_Motor.position_factor = _2DOF_Motor.distance / -115.0;

	// Go to the Q1 minimum position
	start_motor_dec(&_1DOF_Motor);

	while ((HAL_GPIO_ReadPin(WK625_L_GPIO_Port, WK625_L_Pin) == GPIO_PIN_SET))
	{
	}

	// Stop motor
	stop_motor_hard(&_1DOF_Motor);
	Data->limit_switch_L = 1;
	__HAL_TIM_SET_COUNTER(_1DOF_Motor.encoder_htim, 0);
	_1DOF_Motor.start_position = __HAL_TIM_GET_COUNTER(_1DOF_Motor.encoder_htim);
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	HAL_Delay(2000);

	// Go to the Q1 maximum position
	start_motor_inc(&_1DOF_Motor);
	Data->limit_switch_L = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	while ((HAL_GPIO_ReadPin(WK625_R_GPIO_Port, WK625_R_Pin) == GPIO_PIN_SET))
	{
	}

	// Stop motor
	stop_motor_hard(&_1DOF_Motor);
	Data->limit_switch_R = 1;
	HAL_Delay(500);
	_1DOF_Motor.end_position = __HAL_TIM_GET_COUNTER(_1DOF_Motor.encoder_htim);
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	_1DOF_Motor.distance = abs(_1DOF_Motor.start_position) + abs(_1DOF_Motor.end_position - 145);

	_1DOF_Motor.position_factor = _1DOF_Motor.distance / 180.0;
}

