/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "crc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "circ_buff.h"
#include "motors.h"
#include "servos.h"
#include "TCS3200.h"
#include "communication.h"
#include "control.h"
#include "pid.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

uint8_t buffer_uart_rx[RX_SIZE] = { 0 };
uint8_t buffer_uart_tx[TX_SIZE];
uint8_t tmp_buffer_uart_tx[TX_SIZE] = { 'X', ' ', '0', ' ', '0', ' ', '0', ' ', '0', ' ', '0', ' ', '0', ' ', '0', ' ', '0', ' ', '0', ' ', '0', ' ', '0', ' ', '0', ' ', 'n' };
uint8_t buffer[RX_SIZE] = { 0 };

uint16_t crc = 0;

circ_buff_hw_t cb_hw;
circ_buff_t cb;

data_to_transmit data = { 0 };

motor _1DOF_Motor = { 0 };
motor _2DOF_Motor = { 0 };

servo _GRIPPER_Servo = { 0 };
servo _3DOF_Servo = { 0 };

tcs3200 Color_Sensor = { 0 };

inverse_kinematics Inverse_Kinematics_Data = { 0 };

volatile uint32_t IC_Val1 = 0;
volatile uint32_t IC_Val2 = 0;
volatile uint32_t Difference = 0;
volatile uint8_t Is_First_Captured = 0;

volatile uint8_t color_flag = 0;
volatile uint8_t _10ms_flag = 0;
volatile uint8_t _pid_flag = 0;

int len = 0;

uint8_t calibration_flag = 0;

pid PID_Q1;
pid PID_Q2;

uint8_t auto_cycles_cnt = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	circ_buff_hw_isr_tx_completed(&cb_hw, &huart2);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == E18DNK_R_Pin)
	{
		data.cnt_r++;
	}

	if (GPIO_Pin == E18DNK_G_Pin)
	{
		data.cnt_g++;
	}

	if (GPIO_Pin == E18DNK_B_Pin)
	{
		data.cnt_b++;
	}
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	if (htim == &htim3)
	{
		update_motor_position(&_1DOF_Motor, &data);
	}

	if (htim == &htim4)
	{
		update_motor_position(&_2DOF_Motor, &data);
	}

	if (htim == &htim8)
	{
		read_frequency(&Color_Sensor);
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim == &htim6)
	{
		sweep_filters(&Color_Sensor);
		_10ms_flag = 1;
	}

	if (htim == &htim7)
	{
		_pid_flag = 1;
	}
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_TIM2_Init();
	MX_DMA_Init();
	MX_TIM3_Init();
	MX_USART2_UART_Init();
	MX_TIM1_Init();
	MX_TIM4_Init();
	MX_TIM8_Init();
	MX_TIM6_Init();
	MX_ADC1_Init();
	MX_CRC_Init();
	MX_TIM7_Init();
	/* USER CODE BEGIN 2 */
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);

	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);

	HAL_TIM_Encoder_Start_IT(&htim3, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start_IT(&htim4, TIM_CHANNEL_ALL);

	HAL_TIM_IC_Start_IT(&htim8, TIM_CHANNEL_4);

	HAL_TIM_Base_Start_IT(&htim6);
	HAL_TIM_Base_Start_IT(&htim7);

	// Initialize PID

	// Initialize motors
	_1DOF_Motor = (motor ) { .pwm_channel = TIM_CHANNEL_1, .pwm_htim = &htim1, .encoder_htim = &htim3 };
	_2DOF_Motor = (motor ) { .pwm_channel = TIM_CHANNEL_2, .pwm_htim = &htim1, .encoder_htim = &htim4 };

	// Initialize servos
	_GRIPPER_Servo = (servo ) { .pwm_channel = TIM_CHANNEL_1, .pwm_htim = &htim2 };
	_3DOF_Servo = (servo ) { .pwm_channel = TIM_CHANNEL_2, .pwm_htim = &htim2 };

	// Initialize TCS3200 color sensor
	Color_Sensor = (tcs3200 ) { .input_capture_htim = &htim8, .input_capture_channel = TIM_CHANNEL_4 };

	// Initialize data
	data = (data_to_transmit ) { .limit_switch_R = 0, .limit_switch_L = 0, .limit_switch_U = 0, .limit_switch_D = 0, .detected_color = 'n' };
	data = (data_to_transmit ) { .cnt_r = 0, .cnt_g = 0, .cnt_b = 0 };

	// Set motors speed
	set_motor_speed(&_1DOF_Motor, 30);
	set_motor_speed(&_2DOF_Motor, 100);

	// Set servos
	open_gripper(&_GRIPPER_Servo, &data);
	set_3DOF_start_angle(&_3DOF_Servo);

	// Set TCS3200 color sensor
	set_output_frequency_scale(&Color_Sensor, 20);
	set_filter_diode(&Color_Sensor, 'r');

	// Initialize circular buffer
	circ_buff_hw_init(&cb_hw, &hdma_usart2_rx, &huart2);
	circ_buff_init(&cb, &cb_hw, buffer_uart_rx, RX_SIZE, 50);
	circ_buff_start_rx(&cb);
	circ_buff_start_tx_buff(&cb, buffer_uart_tx, TX_SIZE);

	// Initialize ADC_DMA
	HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*) data.fsr402_value, 2);

	// Initialize data frame
	update_data_to_transmit(&data, buffer_uart_tx, tmp_buffer_uart_tx, &hcrc);

	//Initialize PID
	pid_init(&PID_Q1, 1.15, 0.00002, 0.05, 35, 50);
	pid_init(&PID_Q2, 1.15, 0.00002, 0.05, 50, 100);

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		if (circ_buff_new_data(&cb))
		{
			len = circ_buff_new_data_amount(&cb);
			if (len > 0)
			{
				circ_buff_read(&cb, buffer, len);
			}
		}

		// Require calibration
		if (buffer[0] == 'C' && buffer[1] == '1' && calibration_flag == 0)
		{
			calibrate(&data, buffer_uart_tx, tmp_buffer_uart_tx, &hcrc);
			calibration_flag = 1;
		}

		if (calibration_flag == 1)
		{
			// Manual control
			manual_motor_control(buffer, &data);
			manual_gripper_control(buffer, &data);
			manual_3DOF_control(&_3DOF_Servo, buffer);
			update_3DOF_angle(&_3DOF_Servo, &data);
			detect_color(&Color_Sensor, &data);
			set_motors_speed_rx(buffer);

			if (_pid_flag == 1 && buffer[0] == 'I') // Inverse kinematics control
			{
				_pid_flag = 0;

				parse_inverse_kinematics_data(buffer, &Inverse_Kinematics_Data);
				calculate_inverese_kinematics(&Inverse_Kinematics_Data);
				calculate_PID(&PID_Q1, _1DOF_Motor.position, Inverse_Kinematics_Data.q1_target_pos);
				calculate_PID(&PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data.q2_target_pos);
				moveQ1(&PID_Q1);
				moveQ2(&PID_Q2);
				moveQ3(Inverse_Kinematics_Data.q3_target_pos);
			}

			if (buffer[0] == 'A' && buffer[1] == '0') // Reset automatic cycles
			{
				auto_cycles_cnt = 0;
			}

			if (buffer[0] == 'A' && buffer[1] == '1' && auto_cycles_cnt < 3) // Auto mode
			{
				automatic_mode(&PID_Q1, &PID_Q2, &data, &Inverse_Kinematics_Data, buffer_uart_tx, tmp_buffer_uart_tx, &hcrc);
				update_data_to_transmit(&data, buffer_uart_tx, tmp_buffer_uart_tx, &hcrc);
				auto_cycles_cnt++;
			}

			if (buffer[0] == 'T' && buffer[1] == '1' && auto_cycles_cnt < 1) // Trajectory mode
			{
				trajectory_mode(&PID_Q1, &PID_Q2, &data, &Inverse_Kinematics_Data, buffer_uart_tx, tmp_buffer_uart_tx, &hcrc);
				update_data_to_transmit(&data, buffer_uart_tx, tmp_buffer_uart_tx, &hcrc);
				auto_cycles_cnt++;
			}

		}

		if (_10ms_flag == 1)
		{
			update_data_to_transmit(&data, buffer_uart_tx, tmp_buffer_uart_tx, &hcrc);
			_10ms_flag = 0;
		}

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		Error_Handler();
	}

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 10;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
	{
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
