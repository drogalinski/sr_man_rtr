#include "control.h"

void parse_inverse_kinematics_data(uint8_t *buffer_rx, inverse_kinematics *Inverse_Kinematics_Data)
{

	char tmp[64] = { 0 };
	strcpy(tmp, (char*) buffer_rx);
	memmove(tmp, tmp + 2, strlen(tmp));
	sscanf(tmp, "%f %f %d", &Inverse_Kinematics_Data->recieved_X, &Inverse_Kinematics_Data->recieved_Z, &Inverse_Kinematics_Data->recieved_fi);

}

void calculate_inverese_kinematics(inverse_kinematics *Inverse_Kinematics_Data)
{

	double alfa = 0.0;
	alfa = asin(Inverse_Kinematics_Data->recieved_X / ARM_LENGTH) * 180 / M_PI;

	Inverse_Kinematics_Data->q1_target_pos = _1DOF_Motor.position_factor * (alfa + 90);
	Inverse_Kinematics_Data->q2_target_pos = _2DOF_Motor.position_factor * (Inverse_Kinematics_Data->recieved_Z - 55);
	Inverse_Kinematics_Data->q3_target_pos = abs((Inverse_Kinematics_Data->recieved_fi * ANGLE_FACTOR) - _3DOF_START_POS);

}

void moveQ1(pid *PID)
{

	if ((int) PID->control_signal < 5 && (int) PID->control_signal > -5)
	{
		PID->motor_direction = 0;
	}
	else if (PID->control_signal < 0)
	{
		PID->motor_direction = -1;
	}
	else if (PID->control_signal > 0)
	{
		PID->motor_direction = 1;
	}

	PID->PWM_value = (int) fabs(PID->control_signal);

	if (PID->PWM_value > PID->max_control_signal)
	{
		PID->PWM_value = PID->max_control_signal;
	}

	if (PID->PWM_value < PID->min_control_signal && PID->error_value != 0)
	{
		PID->PWM_value = PID->min_control_signal;
	}

	if (PID->motor_direction == -1)
	{
		start_motor_dec(&_1DOF_Motor);
	}
	else if (PID->motor_direction == 1)
	{
		start_motor_inc(&_1DOF_Motor);
	}
	else
	{
		stop_motor_hard(&_1DOF_Motor);
		PID->PWM_value = 0;

	}
	set_motor_speed(&_1DOF_Motor, PID->PWM_value);

}

void moveQ2(pid *PID)
{

	if ((int) PID->control_signal < 5 && (int) PID->control_signal > -5)
	{
		PID->motor_direction = 0;
	}
	else if (PID->control_signal < 0)
	{
		PID->motor_direction = -1;
	}
	else if (PID->control_signal > 0)
	{
		PID->motor_direction = 1;
	}

	PID->PWM_value = (int) fabs(PID->control_signal);

	if (PID->PWM_value > PID->max_control_signal)
	{
		PID->PWM_value = PID->max_control_signal;
	}

	if (PID->PWM_value < PID->min_control_signal && PID->error_value != 0)
	{
		PID->PWM_value = PID->min_control_signal;
	}

	if (PID->motor_direction == -1)
	{
		start_motor_inc(&_2DOF_Motor);
	}
	else if (PID->motor_direction == 1)
	{
		start_motor_dec(&_2DOF_Motor);
	}
	else
	{
		stop_motor_hard(&_2DOF_Motor);
		PID->PWM_value = 0;

	}
	set_motor_speed(&_2DOF_Motor, PID->PWM_value);
}

void moveQ3(int target_pos)
{
	_3DOF_Servo.pwm_duty = target_pos;
	__HAL_TIM_SET_COMPARE(_3DOF_Servo.pwm_htim, _3DOF_Servo.pwm_channel, (uint32_t )_3DOF_Servo.pwm_duty);
}

void automatic_mode(pid *PID_Q1, pid *PID_Q2, data_to_transmit *Data, inverse_kinematics *Inverse_Kinematics_Data, uint8_t *buffer_tx, uint8_t *tmp_buffer_tx, CRC_HandleTypeDef *hcrc)
{
	uint8_t detected_color = 0;
	int counter_Q1 = 0;
	int counter_Q2 = 0;

	// Go above block pick up position
	Inverse_Kinematics_Data->recieved_X = -84.0;
	Inverse_Kinematics_Data->recieved_Z = 150.0;
	Inverse_Kinematics_Data->recieved_fi = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);
	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 2000);

	counter_Q2 = 0;

	do
	{
		calculate_PID(PID_Q1, _1DOF_Motor.position, Inverse_Kinematics_Data->q1_target_pos);
		moveQ1(PID_Q1);
		HAL_Delay(1);
		counter_Q1++;
	} while (counter_Q1 != 2000);

	counter_Q1 = 0;
	moveQ3(Inverse_Kinematics_Data->q3_target_pos);
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	// Wait for block placement
	do
	{
		detect_color(&Color_Sensor, Data);

	} while (Data->detected_color == 'n');

	HAL_Delay(500);

	// Wait for block placement
	do
	{
		detect_color(&Color_Sensor, Data);

	} while (Data->detected_color == 'n');

	// Save detected color
	detected_color = Data->detected_color;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	// Wait for 2 seconds
	HAL_Delay(2000);

	// Go to the block pick up position
	Inverse_Kinematics_Data->recieved_X = -84.0;
	Inverse_Kinematics_Data->recieved_Z = 56.0;
	Inverse_Kinematics_Data->recieved_fi = 0;
	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 2000);

	counter_Q2 = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	// Wait for 1 seconds
	HAL_Delay(1000);

	// Close gripper
	do
	{
		control_close_gripper_auto(&_GRIPPER_Servo, Data);
	} while (_GRIPPER_Servo.gripper_flag != 1);

	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	// Wait for 1 second
	HAL_Delay(1000);

	// Go above block pick up position
	Inverse_Kinematics_Data->recieved_X = -84.0;
	Inverse_Kinematics_Data->recieved_Z = 125.0;
	Inverse_Kinematics_Data->recieved_fi = 0;
	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 2000);

	counter_Q2 = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	if (detected_color == 'r') // Go to red block drop position
	{

		Inverse_Kinematics_Data->recieved_X = -116.5;
		Inverse_Kinematics_Data->recieved_Z = 65.0;
		Inverse_Kinematics_Data->recieved_fi = 0;

	}
	else if (detected_color == 'g') // Go to green block drop position
	{

		Inverse_Kinematics_Data->recieved_X = 0.0;
		Inverse_Kinematics_Data->recieved_Z = 65.0;
		Inverse_Kinematics_Data->recieved_fi = 0;
	}
	else if (detected_color == 'b') // Go to blue block drop position
	{
		// Go to blue block drop position
		Inverse_Kinematics_Data->recieved_X = 117.5;
		Inverse_Kinematics_Data->recieved_Z = 65.0;
		Inverse_Kinematics_Data->recieved_fi = 0;
	}

	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q1, _1DOF_Motor.position, Inverse_Kinematics_Data->q1_target_pos);
		moveQ1(PID_Q1);
		HAL_Delay(1);
		counter_Q1++;
	} while (counter_Q1 != 1500);

	counter_Q1 = 0;

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 1500);

	counter_Q2 = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);

	// Open gripper
	open_gripper(&_GRIPPER_Servo, Data);
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);
}

void trajectory_mode(pid *PID_Q1, pid *PID_Q2, data_to_transmit *Data, inverse_kinematics *Inverse_Kinematics_Data, uint8_t *buffer_tx, uint8_t *tmp_buffer_tx, CRC_HandleTypeDef *hcrc)
{
	int counter_Q1 = 0;
	int counter_Q2 = 0;

	Inverse_Kinematics_Data->recieved_X = -83.1;
	Inverse_Kinematics_Data->recieved_Z = 70.0;
	Inverse_Kinematics_Data->recieved_fi = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);
	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 2000);

	counter_Q2 = 0;

	do
	{
		calculate_PID(PID_Q1, _1DOF_Motor.position, Inverse_Kinematics_Data->q1_target_pos);
		moveQ1(PID_Q1);
		HAL_Delay(1);
		counter_Q1++;
	} while (counter_Q1 != 2000);

	counter_Q1 = 0;

	Inverse_Kinematics_Data->recieved_X = 83.1;
	Inverse_Kinematics_Data->recieved_Z = 90.0;
	Inverse_Kinematics_Data->recieved_fi = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);
	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q1, _1DOF_Motor.position, Inverse_Kinematics_Data->q1_target_pos);
		moveQ1(PID_Q1);
		HAL_Delay(1);
		counter_Q1++;
	} while (counter_Q1 != 2000);

	counter_Q1 = 0;

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 2000);

	counter_Q2 = 0;

	Inverse_Kinematics_Data->recieved_X = -83.1;
	Inverse_Kinematics_Data->recieved_Z = 110.0;
	Inverse_Kinematics_Data->recieved_fi = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);
	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q1, _1DOF_Motor.position, Inverse_Kinematics_Data->q1_target_pos);
		moveQ1(PID_Q1);
		HAL_Delay(1);
		counter_Q1++;
	} while (counter_Q1 != 2000);

	counter_Q1 = 0;

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 2000);

	counter_Q2 = 0;

	Inverse_Kinematics_Data->recieved_X = 83.1;
	Inverse_Kinematics_Data->recieved_Z = 130.0;
	Inverse_Kinematics_Data->recieved_fi = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);
	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q1, _1DOF_Motor.position, Inverse_Kinematics_Data->q1_target_pos);
		moveQ1(PID_Q1);
		HAL_Delay(1);
		counter_Q1++;
	} while (counter_Q1 != 2000);

	counter_Q1 = 0;

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 2000);

	counter_Q2 = 0;

	Inverse_Kinematics_Data->recieved_X = -83.1;
	Inverse_Kinematics_Data->recieved_Z = 150.0;
	Inverse_Kinematics_Data->recieved_fi = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);
	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q1, _1DOF_Motor.position, Inverse_Kinematics_Data->q1_target_pos);
		moveQ1(PID_Q1);
		HAL_Delay(1);
		counter_Q1++;
	} while (counter_Q1 != 2000);

	counter_Q1 = 0;

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 2000);

	counter_Q2 = 0;

	Inverse_Kinematics_Data->recieved_X = 0;
	Inverse_Kinematics_Data->recieved_Z = 150.0;
	Inverse_Kinematics_Data->recieved_fi = 0;
	update_data_to_transmit(Data, buffer_tx, tmp_buffer_tx, hcrc);
	calculate_inverese_kinematics(Inverse_Kinematics_Data);

	do
	{
		calculate_PID(PID_Q1, _1DOF_Motor.position, Inverse_Kinematics_Data->q1_target_pos);
		moveQ1(PID_Q1);
		HAL_Delay(1);
		counter_Q1++;
	} while (counter_Q1 != 2000);

	counter_Q1 = 0;

	do
	{
		calculate_PID(PID_Q2, _2DOF_Motor.position, Inverse_Kinematics_Data->q2_target_pos);
		moveQ2(PID_Q2);
		HAL_Delay(1);
		counter_Q2++;
	} while (counter_Q2 != 2000);

	counter_Q2 = 0;

}
